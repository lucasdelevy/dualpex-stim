#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  pwm_pigpio.py
#  
#  Copyright 2015  <pi@raspberrypi>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

import pigpio as gpio
import time

# User configuration
PWM_DC = 0.5
PWM_MODE = 'rect'
NEG_PW = 500e-6 # secs
INTER_PULSE_PW = 50e-6 # secs
POS_PW = 500e-6 # secs
INTER_PHASE_PW = 20e-3 #secs


# Initialization
PWM_PIN = 24
NEG_PIN = 23
POS_PIN = 18
PWM_RANGE = 512
LOW = 0
HIGH = 1

def main():
	# Initialize system variables
	status = 'first_pulse'
	old_status = ''
	
	# Setup pins
	pi1 = gpio.pi()
	pi1.set_mode(PWM_PIN, gpio.OUTPUT)
	pi1.set_mode(NEG_PIN, gpio.OUTPUT)
	pi1.set_mode(POS_PIN, gpio.OUTPUT)

	# Initialize pulses
	pi1.write(NEG_PIN, LOW)
	pi1.write(POS_PIN, LOW)
	
	# Initialize PWM
	dc = PWM_DC * PWM_RANGE if PWM_DC * PWM_RANGE < 255 else 255
	f = pi1.set_PWM_frequency(PWM_PIN, 40000)
	print f
	print pi1.get_PWM_frequency(PWM_PIN)
	pi1.set_PWM_dutycycle(PWM_PIN, dc)
	
	pi1.write(NEG_PIN, HIGH)
	pi1.write(POS_PIN, LOW)
	t0 = 0
	# Main loop
	try:
		while True:
			if old_status != status:
				print time.time() - t0
				t0 = time.time()
				old_status = status
				
			if status == 'first_pulse':
				dt = time.time() - t0
				dc = getDutyCycle(PWM_MODE, dt)
				
				if dt >= NEG_PW:
					status = 'inter_phase'
					pi1.write(NEG_PIN, LOW)
					pi1.write(POS_PIN, LOW)
			elif status == 'inter_phase':
				dt = time.time() - t0
				
				if dt >= INTER_PHASE_PW:
					status = 'second_pulse'
					pi1.write(NEG_PIN, LOW)
					pi1.write(POS_PIN, HIGH)
			elif status == 'second_pulse':
				dt = time.time() - t0
				dc = getDutyCycle(PWM_MODE, dt)
				
				if dt >= POS_PW:
					status = 'inter_pulse'
					pi1.write(NEG_PIN, LOW)
					pi1.write(POS_PIN, LOW)
			elif status == 'inter_pulse':
				dt = time.time() - t0
				
				if dt >= INTER_PULSE_PW:
					status = 'first_pulse'
					pi1.write(NEG_PIN, HIGH)
					pi1.write(POS_PIN, LOW)
	except KeyboardInterrupt:
		pi1.set_PWM_dutycycle(PWM_PIN, 0)
		pi1.stop()
	
	return 0

def getDutyCycle(mode, t):
	
	if mode == 'rect':
		dc = PWM_DC * PWM_RANGE if PWM_DC * PWM_RANGE < 255 else 255
		
	
	return dc

if __name__ == '__main__':
	main()

