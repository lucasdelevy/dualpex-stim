import time
import threading
import pigpio as gpio

NEG_PW = 100e-6
POS_PW = 100e-6

p = gpio.pi()
p.set_mode(23,gpio.OUTPUT)

exit_flag = threading.Event()

try:
	while True:
		p.write(23,1)
		while exit_flag.wait(timeout = NEG_PW):
			pass
				
		p.write(23,0)
		while exit_flag.wait(timeout = POS_PW):
			pass
		
except KeyboardInterrupt:
	p.write(23, 0)
