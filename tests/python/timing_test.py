import sys
import threading
import time
import pigpio as gpio

def thread1():
	global flag1, OUT_PIN
	
	old_flag1 = 2
	
	while 1:
		if old_flag1 != flag1:
			p.write(18,flag1)
			old_flag1 = flag1
			
			sys.stdout.write(flag1 + '\n')

global flag1
flag1 = 0

p = gpio.pi()
p.set_mode(18,gpio.OUTPUT)

t1 = threading.Thread(target = thread1)

#p.write(23,1)
#time.sleep(0.0001)
#p.write(23,0)

i = 0
while i < 10: 
	t0 = time.time()
	flag1 = 1
	
	while time.time() - t0 >= 0.1:
		pass
	
	t1 = time.time()	
	flag1 = 0
	
	while time.time() - t1 >= 0.1:
		pass
	
	#print time.time()-t0
	
	i = i + 1
