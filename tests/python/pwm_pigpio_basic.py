#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  untitled.py
#  
#  Copyright 2015  <pi@raspberrypi>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

import pigpio as gpio
import time


def main():
	pwmPin = 24
	pulseNeg = 23
	pulsePos = 18

	dc = 50
	
	pi1 = gpio.pi();
	pi1.set_mode(pwmPin, gpio.OUTPUT);
	#pi1.set_mode(pulseNeg, pigpio.OUTPUT);
	#pi1.set_mode(pulsePos, pigpio.OUTPUT);

	#pi1.write(pulseNeg, 0);
	#pi1.write(pulsePos, 0);
	
	f = pi1.set_PWM_frequency(pwmPin, 40000)
	print f
	print pi1.get_PWM_frequency(pwmPin)
	pi1.set_PWM_dutycycle(pwmPin, 128)
	
	#raw_input('Press ENTER to finish')
	
	#pi1.set_PWM_dutycycle(pwmPin, 0)
	#pi1.stop()
	#try:
	while 1:
		#pass			
		#i = raw_input('DC = ')
		#pwm.ChangeDutyCycle(float(i))
		
		pi1.write(pulseNeg, 1)
		time.sleep(0.0005)
		pi1.write(pulseNeg, 0)
		pi1.write(pulsePos, 1)
		time.sleep(0.0005)
		pi1.write(pulsePos, 0)
		
		time.sleep(0.008)
	#except KeyboardInterrupt:
		#pwm.stop()
		#gpio.cleanup()
	
	return 0

if __name__ == '__main__':
	main()

