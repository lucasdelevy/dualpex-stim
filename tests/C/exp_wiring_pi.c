#include <wiringPi.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include <unistd.h>
#include <termios.h>
#include <signal.h>
#include <math.h>

#define PWM_PIN 1
#define NEG_PIN 4
#define POS_PIN 5

/* User settings */
#define RANGE 512
#define STD_DUTY_CYCLE 0.5
#define NEG_PW 500 // us
#define POS_PW 500 // us
#define INTERPHASE_DELAY 50 // us
#define INTERPULSE_DELAY 2000 // us
#define NEG_TIME_OFFSET 50 // us
#define POS_TIME_OFFSET 50 // us
#define PWM_MODE 1 // 0: rectangular; 1: exponential

/* Initialization */
#define PWM_PERIOD 0.00005

enum pulse_status_t
{
	before_first_pulse,
	first_pulse,
	inter_phase,
	before_second_pulse,
	second_pulse,
	inter_pulse,
	none
} old_status, curr_status;

volatile sig_atomic_t ctrld_pressed = 0;

int getDutyCycle(int dt, int mode);

void ctrld(int sig)
{
	/* Exiting program */
	printf("\nExiting...\n");
	digitalWrite(NEG_PIN, LOW);
	digitalWrite(POS_PIN, LOW);
	pwmWrite(PWM_PIN,0);
	
	exit(1);
}

int main()
{
	int dc = 0;
	int t_this_status = 0;
	int t_this_PWM = 0;
	old_status = none;
	curr_status = inter_pulse;
	
	printf("Starting pulse test...\n");
	
	if (wiringPiSetup() == -1)
		exit(1);
		
	/* Initialize digital outputs */
	pinMode(NEG_PIN, OUTPUT);
	pinMode(POS_PIN, OUTPUT);
		
	/* Initialize PWM */
	pinMode(PWM_PIN, PWM_OUTPUT);
	pwmSetRange(RANGE);
	pwmSetMode(PWM_MODE_MS);
	pwmSetClock(2);
	
	pwmWrite(PWM_PIN,dc);
	
	/* Main loop */
	signal(SIGINT, ctrld); // keyboard input changes variable
	while (1)
	{
		if (old_status != curr_status)
		{
			t_this_status = micros();
			old_status = curr_status;
		}
		/* A little before first pulse */
		if (curr_status == before_first_pulse)
		{
			pwmWrite(PWM_PIN, (int)STD_DUTY_CYCLE*RANGE);
			delayMicroseconds(NEG_TIME_OFFSET);
			
			curr_status = first_pulse;
		}
		/* First pulse */
		if (curr_status == first_pulse)
		{
			digitalWrite(NEG_PIN, HIGH);
			t_this_PWM = 0;
			while ((micros() - t_this_status) < NEG_PW)
			{
				if ((micros() - t_this_PWM) > PWM_PERIOD)
				{
					dc = getDutyCycle((micros() - t_this_status), PWM_MODE);
					pwmWrite(PWM_PIN, dc);
					t_this_PWM = micros();
				}
			}
			digitalWrite(NEG_PIN, LOW);	
			
			curr_status = inter_phase;
		}
		/* Interphase */
		else if (curr_status == inter_phase)
		{
			pwmWrite(PWM_PIN, 0);
			delayMicroseconds(INTERPHASE_DELAY);
			
			curr_status = inter_pulse; // before_second_pulse
		}
		/* A little before second pulse */
		if (curr_status == before_second_pulse)
		{
			pwmWrite(PWM_PIN, (int)STD_DUTY_CYCLE*RANGE);
			delayMicroseconds(POS_TIME_OFFSET);
			
			curr_status = second_pulse;
		}
		/* Second pulse */
		else if (curr_status == second_pulse)
		{
			digitalWrite(POS_PIN, HIGH);
			t_this_PWM = 0;
			while ((micros() - t_this_status) < POS_PW)
			{
				if ((micros() - t_this_PWM) > PWM_PERIOD)
				{
					dc = getDutyCycle((micros() - t_this_status), PWM_MODE);
					pwmWrite(PWM_PIN, dc);
					t_this_PWM = micros();
				}
			}
			digitalWrite(POS_PIN, LOW);
			
			curr_status = inter_pulse;
		}
		/* Interpulse */
		else if (curr_status == inter_pulse)
		{
			pwmWrite(PWM_PIN, 0);
			delayMicroseconds(INTERPULSE_DELAY);
			
			curr_status = before_first_pulse;
		}
	}
	
	return 0;
}

#define RC_CANCEL_FUNC (RANGE * (1-exp(-.0001*dt)))
#define RC_CANCEL_AND_MOD_FUNC (RANGE * (1-exp(-.00001*dt)))
//#define DESIRED_FUNC (-RANGE * exp(.00005*dt))

int getDutyCycle(int dt, int mode)
{
	if (mode == 0) // rectangular
		return RANGE*0.5;
	else if (mode == 1) // exponential
		return 0;
}
