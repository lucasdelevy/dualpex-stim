#include <wiringPi.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include <unistd.h>
#include <termios.h>
#include <signal.h>

#define PWM_PIN 1
#define NEG_PIN 4
#define POS_PIN 5

/* User settings */
#define RANGE 512
#define DUTY_CYCLE 0.5
#define NEG_PW 50 // us
#define POS_PW 50 // us
#define INTERPHASE_DELAY 50 // us
#define INTERPULSE_DELAY 2000 // us
//#define INTERPULSE_DELAY 20000 - POS_PW - NEG_PW - INTERPHASE_DELAY

volatile sig_atomic_t ctrld_pressed = 0;

void ctrld(int sig)
{
	/* Exiting program */
	printf("\nExiting...\n");
	digitalWrite(NEG_PIN, LOW);
	digitalWrite(POS_PIN, LOW);
	pwmWrite(PWM_PIN,0);
	
	exit(1);
}

int main()
{
	int dc = DUTY_CYCLE * RANGE;
	int t0 = 0;
	int t1 = 0;
	
	printf("Starting pulse test...\n");
	
	if (wiringPiSetup() == -1)
		exit(1);
		
	/* Initialize digital outputs */
	pinMode(NEG_PIN, OUTPUT);
	pinMode(POS_PIN, OUTPUT);
		
	/* Initialize PWM */
	pinMode(PWM_PIN, PWM_OUTPUT);
	pwmSetRange(RANGE);
	pwmSetMode(PWM_MODE_MS);
	pwmSetClock(8);
	
	pwmWrite(PWM_PIN,dc);
	
	/* Main loop */
	signal(SIGINT, ctrld); // keyboard input changes variable
	while(1)
	{
		t0 = micros();
		
		/* First pulse */
		digitalWrite(NEG_PIN, HIGH);
		delayMicroseconds(NEG_PW);
		digitalWrite(NEG_PIN, LOW);
	
		//pwmWrite(PWM_PIN, 128);
		delayMicroseconds(INTERPHASE_DELAY);
		
		/* Second pulse */
		digitalWrite(POS_PIN, HIGH);
		delayMicroseconds(POS_PW);
		digitalWrite(POS_PIN, LOW);
				
		t1 = micros();
		//printf("%d ", (t1-t0));
		
		//pwmWrite(PWM_PIN,dc);
		delayMicroseconds((INTERPULSE_DELAY));
	}
	
	return 0;
}
