#include <pigpio.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include <unistd.h>
#include <termios.h>
#include <signal.h>

#define PWM_PIN 24
#define NEG_PIN 23
#define POS_PIN 18

/* User settings */
#define RANGE 256
#define DUTY_CYCLE 0.5
#define NEG_PW 500
#define POS_PW 500
#define INTERPHASE_DELAY 50
#define INTERPULSE_DELAY 20000

volatile sig_atomic_t ctrld_pressed = 0;

void ctrlc(int sig)
{
	/* Exiting program */
	printf("\nExiting...\n");
	gpioWrite(NEG_PIN, 0);
	gpioWrite(POS_PIN, 0);
	gpioWrite(PWM_PIN, 0);
	
	gpioTerminate();
	
	exit(1);
}

int main()
{
	int dc = DUTY_CYCLE * RANGE;
	int t0 = 0;
	int t1 = 0;
		
	gpioCfgClock(1,0,0);
	if (gpioInitialise() < 0)
		exit(1);

	printf("Starting pulse test...\n");

	/* Initialize digital outputs */
	gpioSetMode(NEG_PIN, PI_OUTPUT);
	gpioSetMode(POS_PIN, PI_OUTPUT);
		
	/* Initialize PWM */
	//gpioSetMode(PWM_PIN, PI_OUTPUT);
	gpioSetPWMfrequency(PWM_PIN,100000);
	gpioGetPWMfrequency(PWM_PIN);
	gpioPWM(PWM_PIN,dc);
	
	/* Main loop */
	signal(SIGINT, ctrlc); // keyboard input changes variable
	while(1)
	{
		//t0 = micros(); // procurar micros() na time.h
		
		/* First pulse */
		gpioWrite(NEG_PIN, 1);
		gpioDelay(NEG_PW);
		gpioWrite(NEG_PIN, 0);
	
		gpioPWM(PWM_PIN,64);
		//gpioDelay(50);
		
		/* Second pulse */
		gpioWrite(NEG_PIN, 1);
		gpioDelay(NEG_PW);
		gpioWrite(NEG_PIN, 0);
						
		//t1 = micros();
		//printf("%d ", (t1-t0));
		
		gpioPWM(PWM_PIN,dc);
		gpioDelay((INTERPULSE_DELAY-(1050)));
	}
	
	return 0;
}
