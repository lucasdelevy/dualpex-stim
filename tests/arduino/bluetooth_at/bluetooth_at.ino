/* *
 * Code based on Instructables tutorial: http://www.instructables.com/id/Modify-The-HC-05-Bluetooth-Module-Defaults-Using-A/?ALLSTEPS
 * For the HC05 board used, the pushbutton was be pressed while powering it on.
 * And on the reference sheet: http://www.instructables.com/files/orig/FOR/4FP2/HKZAVRT6/FOR4FP2HKZAVRT6.pdf 
 * 
 * This script enables communication with the HC05 board while in AT mode
 * using Arduino IDE's serial monitor.
 *  */

#include <SoftwareSerial.h>

SoftwareSerial BTSerial(9,10);

void setup()
{
  // Changing HC05 to AT mode (if button is not present in board)
//  pinMode(7, OUTPUT);
//  digitalWrite(7, HIGH);
  
  Serial.begin(38400);
  BTSerial.begin(38400);
  Serial.println("Enter AT commands:");
}

void loop()
{
  if (BTSerial.available() > 0)
  {
    byte received_byte = BTSerial.read();
    Serial.write(received_byte);
  }
  if (Serial.available() > 0 )
  {
    byte received_byte = Serial.read();
    BTSerial.write(received_byte);
  }
}
