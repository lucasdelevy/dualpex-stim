#include <String.h>

/* Standard configuration */
#define STD_DUTY_CYCLE 0.5
#define NEG_PW 700 // us
#define POS_PW 500 // us
#define INTERPHASE_DELAY 0 // us (offset de 30us)
#define INTERPULSE_DELAY 2000 // us
#define PWM_MODE 2 // 0: rectangular; 1: exponential; 2: sinusoidal
#define PWM_RANGE 256
#define NEG_TIME_OFFSET 50 // us
#define POS_TIME_OFFSET 50 // us

/* Definitions */
#define NEG_PIN 2
#define POS_PIN 4
#define PWM_PIN 3
#define PWM_PERIOD 33
#define LED_PIN 13

/* Variable declaration */
enum diagnosis_mode_t
{
  chronaxie,
  rheobase,
  accommodation,
  science
} diagnosis_mode;
int leave_loop;

/* First run */
void setup()
{
  /* Variable initialization */
  diagnosis_mode = science;
  leave_loop = 0;

  /* System initialization */
  Serial.begin(9600);
}

/* iteration */
void loop()
{
  startSession();

  // depending on therapy mode, act accordingly
  if (diagnosis_mode == chronaxie)
  {
    // iterate here until result is found
  }
  else if (diagnosis_mode == rheobase)
  {
    // iterate here until result is found
  }
  else if (diagnosis_mode == accommodation)
  {
    // iterate here until result is found
  }
  else
  {
    while (leave_loop == 0)
    {
      // stay here stimulating with parameters
      // get parameters from BlueTooth
    }

    leave_loop = 0;
  }
}

void startSession()
{
  while (leave_loop == 0)
    getBTDiagnosisMode();
  
  leave_loop = 0;
}

void readBTCommand()
{
  if (Serial.available() > 0)
  {
    byte mssg = Serial.read();
    
    // decode the mssg here
  }
}

void getBTDiagnosisMode()
{
  if (Serial.available() > 0)
  {
    String mssg = Serial.readString();

    if (mssg == "start=chronaxie")
    {
      diagnosis_mode = chronaxie;
      Serial.println("OK");
      leave_loop = 1;
    }
    else if (mssg == "start=rheobase")
    {
      diagnosis_mode = rheobase;
      Serial.println("OK");
      leave_loop = 1;
    }
    else if (mssg == "start=accommodation")
    {
      diagnosis_mode = accommodation;
      Serial.println("OK");
      leave_loop = 1;
    }
    else if (mssg == "start=science")
    {
      diagnosis_mode = science;
      Serial.println("OK");
      leave_loop = 1;
    }
  }
}
